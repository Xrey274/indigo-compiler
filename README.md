# README

![indigo-banner](assets/indigo-banner.png)

## About the project

This is a reference compiler, for the indigo language.

## Features

- Support standard gcc compiler flags (future)

## Building

The build script is only meant for bootstrapping and to save you a few seconds of typing.

```
git clone https://gitlab.com/Xrey274/mecha.git
cd Mecha
chmod 777 build
./build
```

## Dependancies

- mecha (needed for building)

- a c++ compiler (g++/clang++ should work)

- c++ stdlib implementation
