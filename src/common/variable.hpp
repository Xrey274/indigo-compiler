#pragma once

#include <cstdint>
#include <string>

#include "symbol.hpp"
#include "type.hpp"

namespace indigo
{
    struct variable
    {
        indigo::type type;
        std::string  name;
        union data
        {
            int32_t i;
            int64_t l;
            float   f;
            double  d;
            int32_t b;
            int8_t  s;
            int64_t p;
            int8_t  v;
        };
    };
};