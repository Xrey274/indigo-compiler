#pragma once

#include <string>

const std::string BOLD_RED = "\33[1;31m";
const std::string BOLD_BLUE = "\33[1;34m";
const std::string BOLD_WHITE = "\33[1;37m";
const std::string RESET = "\33[0m";