#pragma once

#include <string>

namespace indigo
{
	struct token
	{
		std::string  string;
		unsigned 	 line_number;
		unsigned 	 line_column;
	};
}