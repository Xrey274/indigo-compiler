#pragma once

#include "symbol.hpp"
#include "class.hpp"

namespace indigo
{
    struct type
    {
        union
        {
            indigo::primitive primitive;
            indigo::__class __class;
        };
    };
};