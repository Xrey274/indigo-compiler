#pragma once

#include <string>
#include <queue>

#include "type.hpp"

namespace indigo
{
    struct function
    {
        indigo::type return_type;
        std::string  name;
        //std::queue<indigo::variable>   parameters;  TO IMPLEMENT
        //std::vector<indigo::instrutions> instructions;  per line instructions to be executed TO IMPLEMENT
    };
};