#pragma once

#include <string>
#include <vector>

#include "token.hpp"

class token_stream
{
    typedef std::vector<indigo::token> vector;

    public:
        token_stream() {};

        token_stream(std::vector<indigo::token>& lexer_tokens)
        {
            // "move" vector
            m_tokens = std::move(lexer_tokens);
        }

        const indigo::token&
        token_stream::get_token()
        {
            indigo::token token{"", 0, 0};

            // if end of vector hasn't been reached
            if(m_tokens.size() - 1 < m_index)
            {
                return m_tokens[m_index++];
            }

            return token;
        }

        const indigo::token&
        token_stream::last_token()
        {
            return m_tokens[m_index];
        }

        unsigned
        token_stream::number_of()
        {
            return m_tokens.size();
        }

    private:
        vector m_tokens;

        unsigned m_index = 0;

};