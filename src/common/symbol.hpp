#pragma once

#include <unordered_map>
#include <string>

namespace indigo
{
    enum class declaration
    {
        _class,
        _struct,
        _interface,
    };

    enum class context
    {
        _extends,
        _implements,
        _reference,
    };

    enum class primitive
    {
        _int,
        _long,
        _float,
        _double,
        _bool,
        _string,
        _pointer,
        _void
    };

    enum class primitive_modifier
    {
        _const,
        _unsigned,
        _threadlocal,
        _abstract,
    };

    enum class arithmetic_operator
    {
        _plus,
        _minus,
        _multiply,
        _divide,
        _modulo
    };

    enum class assignment_operator
    {
        _increment,
        _decrement,
        _equals,
        _plus_self,
        _minus_self,
        _multiply_self,
        _divide_self,
        _modulo_self
    };

    enum class relational_operator
    {
        _equals,
        _NOT,
        _bigger,
        _smaller,
        _bigger_or_equal,
        _smaller_or_equal,
        _AND,
        _OR,
        _max,
        _min
    };

    enum class bitwise_operator
    {
        _and,
        _or,
        _exclusive_or,
        _right_shift,
        _left_shift,
        _ones_complement
    };

    enum class data_operator
    {
        _sizeof,
        _typeof,
        _numberof,
        _offsetof
    };
};

std::unordered_map<std::string, indigo::declaration> declaration_map
{
    {"class", indigo::declaration::_class},
    {"struct", indigo::declaration::_struct},
    {"interface", indigo::declaration::_interface}
};

std::unordered_map<std::string, indigo::context> context_map
{
    {"extends", indigo::context::_extends},
    {"implements", indigo::context::_implements},
    {"reference", indigo::context::_reference}
};

std::unordered_map<std::string, indigo::primitive> primitive_map
{
    {"int", indigo::primitive::_int},
    {"long", indigo::primitive::_long},
    {"float", indigo::primitive::_float},
    {"double", indigo::primitive::_double},
    {"bool", indigo::primitive::_bool},
    {"string", indigo::primitive::_string},
    {"pointer", indigo::primitive::_pointer},
    {"void", indigo::primitive::_void}
};

std::unordered_map<std::string, indigo::primitive_modifier> primitive_modifier_map
{
    {"const", indigo::primitive_modifier::_const},
    {"unsigned", indigo::primitive_modifier::_unsigned},
    {"threadlocal", indigo::primitive_modifier::_threadlocal},
    {"abstract", indigo::primitive_modifier::_abstract}
};

std::unordered_map<std::string, indigo::arithmetic_operator> arithmetic_operator_map
{
    {"+", indigo::arithmetic_operator::_plus},
    {"-", indigo::arithmetic_operator::_minus},
    {"*", indigo::arithmetic_operator::_multiply},
    {"/", indigo::arithmetic_operator::_divide},
    {"%", indigo::arithmetic_operator::_modulo}
};

std::unordered_map<std::string, indigo::assignment_operator> assignment_operator_map
{
    {"++", indigo::assignment_operator::_increment},
    {"--", indigo::assignment_operator::_decrement},
    {"=", indigo::assignment_operator::_equals},
    {"+=", indigo::assignment_operator::_plus_self},
    {"-=", indigo::assignment_operator::_minus_self},
    {"*=", indigo::assignment_operator::_multiply_self},
    {"/=", indigo::assignment_operator::_divide_self},
    {"%=", indigo::assignment_operator::_modulo_self}
};

std::unordered_map<std::string, indigo::relational_operator> relational_operator_map
{
    {"==", indigo::relational_operator::_equals},
    {"!", indigo::relational_operator::_NOT},
    {">", indigo::relational_operator::_bigger},
    {"<", indigo::relational_operator::_smaller},
    {">=", indigo::relational_operator::_bigger_or_equal},
    {"<=", indigo::relational_operator::_smaller_or_equal},
    {"&&", indigo::relational_operator::_AND},
    {"||", indigo::relational_operator::_OR},
    {"?>", indigo::relational_operator::_max},
    {"?<", indigo::relational_operator::_min}
};

std::unordered_map<std::string, indigo::bitwise_operator> bitwise_operator_map
{
    {"&", indigo::bitwise_operator::_and},
    {"|", indigo::bitwise_operator::_or},
    {"^", indigo::bitwise_operator::_exclusive_or},
    {">>", indigo::bitwise_operator::_right_shift},
    {"<<", indigo::bitwise_operator::_left_shift},
    {"~", indigo::bitwise_operator::_ones_complement}
};

std::unordered_map<std::string, indigo::data_operator> data_operator_map
{
    {"sizeof", indigo::data_operator::_sizeof},
    {"typeof", indigo::data_operator::_typeof},
    {"numberof", indigo::data_operator::_numberof},
    {"offsetof", indigo::data_operator::_offsetof}
};