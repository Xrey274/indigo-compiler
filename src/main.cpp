#include <iostream>
#include <string>
#include <fstream>
#include <thread>

#include "../common/shell_colors.hpp"
#include "timer.hpp"
#include "parser/parser.hpp"

void
start()
{
    try
    {
        indigo::parser new_parser;

        new_parser.parse();
    }
    catch(const std::exception& e)
    {
        std::cerr << BOLD_RED << e.what() << RESET << "\n";

        return;
    }
}

int 
main(int argc, char* argv[])
{
    indigo::timer global_timer;

    global_timer.start();

    start();

    global_timer.stop();

    std::cout << "\n" << BOLD_WHITE << "Elapsed time: " << global_timer.elapsed_time() << RESET << "\n";

    return 0;
}