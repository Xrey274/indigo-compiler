#pragma once

#include <string>
#include <chrono>
#include <sstream>
#include <vector>
#include <cmath>
#include <iostream>

#include "time_struct.hpp"

namespace indigo
{
	enum class time_unit
	{
		millisecond,
		second,
		minute,
		hour
	};

	struct time
	{
		int       amount;
		time_unit unit; 
	};

	class timer
	{
		public:
			timer();

			void start();
			void stop();
			std::string elapsed_time();
		private:
			indigo::time milliseconds{0, time_unit::millisecond};
			indigo::time seconds{0, time_unit::second};
			indigo::time minutes{0, time_unit::minute};
			indigo::time hours{0, time_unit::hour};

			std::chrono::steady_clock::time_point timer_begin;
			std::chrono::steady_clock::time_point timer_end;
	};
};