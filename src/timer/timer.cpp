#include "timer.hpp"

indigo::timer::timer()
{

}

void
indigo::timer::start()
{
	using namespace std::chrono;

	timer_begin = steady_clock::now();
}

void
indigo::timer::stop()
{
	namespace chrono = std::chrono;

	timer_end = chrono::steady_clock::now();

    double end_time   = chrono::time_point_cast<chrono::milliseconds>(timer_end).time_since_epoch().count();
    double start_time = chrono::time_point_cast<chrono::milliseconds>(timer_begin).time_since_epoch().count();

    milliseconds.amount = end_time - start_time;
}

std::string 
indigo::timer::elapsed_time()
{
	using namespace std::chrono;

	// calculate time elapsed in different units
	seconds.amount = milliseconds.amount / 1000;
    minutes.amount = seconds.amount / 60;
    hours.amount   = minutes.amount / 60;
    
	// correct "lower" units by setting them to their remainders
    milliseconds.amount = milliseconds.amount % 1000;
    seconds.amount      = seconds.amount % 60;
    minutes.amount      = minutes.amount % 60;

    std::string elapsed_time;

    if(hours.amount > 0)
    	elapsed_time +=       std::to_string(hours.amount) + " hours";

    if(minutes.amount > 0)
    	elapsed_time += " " + std::to_string(minutes.amount) + " minutes";

    if(seconds.amount > 0)
  		elapsed_time += " " + std::to_string(seconds.amount) + " seconds";

    elapsed_time += " " + std::to_string(milliseconds.amount) + " milliseconds";

    return elapsed_time;
}