#include "lexer.hpp"

lexer::lexer(const std::string file_path)
{
	/*
		Open script and load it into script_string
	*/

	std::ifstream script_file(file_path);
	std::stringstream buffer;
	// load script into buffer
	buffer << script_file.rdbuf();

	script_file.close();

	//if(!script.length()) TO IMPLEMENT

	// reserve buffer based of size of script
	m_script.reserve(std::filesystem::file_size(file_path));
	m_script = buffer.str();

	// add first token
	m_tokens.emplace_back(indigo::token{"", line_number, line_column});

	lex();
}

std::vector<indigo::token>&
lexer::get_vector()
{
	return m_tokens;
}

void
lexer::lex()
{
	bool inside_quotes = false;
	bool inside_comment = false;

	for(unsigned i = 0; i < m_script.length() - 1; ++i)
	{
		add_char(m_script[i], inside_quotes, inside_comment, i);

		line_column += 1;

		if(m_script[i] == '\n')
		{
			// advance line number and reset line column
			line_number += 1;
			line_column =  1;
		}
	}

	// remove last token, which is empty
	if(m_tokens.back().string.empty())
	{
		m_tokens.pop_back();	
	}
}

void
lexer::add_char(char letter, bool& inside_quotes, bool& inside_comment, unsigned& index)
{
	if(inside_comment)
	{
		if(letter == '\n')
		{
			inside_comment = false;
		}
		return;
	}

	// handle new space
	if(letter == ' ' || letter == '\t' || letter == '\n')
	{
		// if last token is not empty, create new one
		if(!m_tokens.back().string.empty())
		{
			m_tokens.emplace_back(indigo::token{"", line_number, line_column});
		}

		return;
	}
	// handle "independant" char (aka. chars that are alone a token)
	else if(letter == '(' || letter == ')' || letter == '{' || letter == '}' || letter == '.' || letter == '"')
	{
		if(inside_quotes == false || letter == '"')
		{
			// put parentheses/braces in seperate tokens
			if(m_tokens.back().string == "")
			{
				m_tokens.back() = indigo::token{std::string(1, letter), line_number, line_column};
			}
			else
			{
				m_tokens.emplace_back(indigo::token{std::string(1, letter), line_number, line_column});
			}

			// create new empty token
			m_tokens.emplace_back(indigo::token{"", line_number, line_column});

			if(letter == '"')
			{
				// flip bool state
				inside_quotes = !inside_quotes;
			}

			return;
		}
	}
	// handle commnets
	else if(letter == '/')
	{
		// if there's a second slash and we don't go out the script's index bounds
		if(m_script.length() > index + 1 && m_script[index + 1] == '/')
		{
			index += 2;
			inside_comment = true;

			return;
		}
	}

	// if other cases haven't returned from function
	m_tokens.back().string += letter;
}