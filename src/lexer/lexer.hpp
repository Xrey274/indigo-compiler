#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <filesystem>
#include <sstream>

#include "../common/token.hpp"

class lexer
{
	public:
		lexer(const std::string);
		std::vector<indigo::token>& get_vector();

	private:
		void lex();
		void add_char(char, bool&, bool&, unsigned&);

		// Variables
		std::string m_script;

		std::vector<indigo::token> m_tokens;

		bool reached_end = false;

		unsigned token_index = 0;
		unsigned line_number = 1;
		unsigned line_column = 1;
};