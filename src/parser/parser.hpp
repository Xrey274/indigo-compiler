#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <utility>
#include <thread>
#include <cstdlib>
#include <cmath>

#include "../lexer/lexer.hpp"
#include "../common/tokenstream.hpp"
#include "../common/token.hpp"
#include "../common/function.hpp"

namespace indigo
{
	class parser
	{
		public:
			parser();

			void parse();

			void detect_function(const indigo::token&);
			void detect_parameters(indigo::function&);
		private:
			token_stream tokens;
	};
};